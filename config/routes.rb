Rails.application.routes.draw do
  
  resources :references
  resources :skills
  resources :honors
  resources :experiences
  resources :generate

  devise_for :users
  root to: "dashboard#index"
  get 'dashboard/index'


  get 'dashboard/experience'
  
  get 'dashboard/honors'

  get 'dashboard/references'

  get 'dashboard/skills'
  
  get 'dashboard/generate'
end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
