class ChangeType < ActiveRecord::Migration[5.0]
 def up
    change_column :courses, :owner, :integer
  end

  def down
    change_column :courses, :owner, :string
  end
end