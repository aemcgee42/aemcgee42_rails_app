class DashboardController < ApplicationController
  def index
    
    if user_signed_in?
      @User = User.find(current_user.id)
      @experiences = Experience.where("owner = ?", current_user.id)
      @honors = Honor.where("owner = ?", current_user.id)
      @references = Reference.where("owner = ?", current_user.id)
      @skills = Skill.where("owner = ?", current_user.id)
    end
  end

  def experience
    @User = User.find(current_user.id)
    @experiences = Experience.where("owner = ?", current_user.id)
  end

  def courses
    @User = User.find(current_user.id)
    @courses = Course.where("owner = ?", current_user.id)
  end
  
  def honors
     @User = User.find(current_user.id)
    @honors = Honor.where("owner = ?", current_user.id)
  end

  def references
     @User = User.find(current_user.id)
    @references = Reference.where("owner = ?", current_user.id)
  end

  def skills
     @User = User.find(current_user.id)
    @skills = Skill.where("owner = ?", current_user.id)
  end
  
  def generate
    user = User.find(current_user.id)
    @exp = Experience.where("owner = ?", current_user.id)
    @hon = Honor.where("owner = ?", current_user.id)
    @ref = Reference.where("owner = ?", current_user.id)
    @ski = Skill.where("owner = ?", current_user.id)
    Caracal::Document.save '/public/example.docx' do |docx|
      
      docx.style do
        id 'Body'
        name 'body'
        font 'Times New Roman'
        size 24
        
      end
    
      
      docx.p do
        style 'Body'
        text user.first_name, size: 48
        text ' '
        text user.last_name, size: 48
        br
        text "Major: ".concat(user.major)
        br
        text "Department: ".concat(user.department)
        br
        text "Degree Program: ".concat(user.degree_program)
      
      end
      docx.hr
      
      docx.h3 'Work Experience'
      @exp.each do | experience |
        docx.p do
          style 'Body'
          text experience.name, bold: true
          text ", ".concat(experience.start_date), italic: true
          text " - ".concat(experience.end_date), italic: true
          br
          text "  ".concat(experience.description)
          ##br

        end
      end
      
      
      docx.h3 'Skills and Interests'
      @ski.each do | skill |
        docx.p do
          style 'Body'
          text skill.skill_name, bold: true
          br
          text " ".concat(skill.skill_description)
          ##br
        end
      end
      
      
      docx.h3 'Honors and Awards'
      @hon.each do | honor |
        docx.p do
          style 'Body'
          text honor.honor_name, bold: true
          text ", ".concat(honor.honor_date), italic: true
          br
          text " ".concat(honor.honor_description)
          ##br
        end
      end
      
      
      docx.h3 'References'
      @ref.each do | reference |
        docx.p do
          style 'Body'
          text reference.reference_name, bold: true
          br
          text " ".concat(reference.reference_contact_info), italic: true
         ## br
        end
      end
      
      
    end
        
        
        path = File.join(Rails.root, "public")
        send_file(File.join(path, "example.docx"))
        
     
    
  end
end
