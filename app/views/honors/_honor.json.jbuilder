json.extract! honor, :id, :owner, :honor_date, :honor_name, :honor_description, :created_at, :updated_at
json.url honor_url(honor, format: :json)