json.extract! skill, :id, :owner, :skill_name, :skill_description, :created_at, :updated_at
json.url skill_url(skill, format: :json)