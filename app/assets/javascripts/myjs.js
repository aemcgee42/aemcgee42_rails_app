

$('document').ready(function()
{
    if($('#experienceList').length)
    {
        var myexpList = document.getElementById("experienceList").getElementsByTagName("tr");
        
        
        for(i=0; i < myexpList.length; i++)
        {
            myexpList[i].addEventListener("click", activateItemExp);
            
        }
    } 
    if($('#honorsList').length)
    {
        var myhonorList = document.getElementById("honorsList").getElementsByTagName("tr");
        for(i=0; i < myhonorList.length; i++)
        {
            myhonorList[i].addEventListener("click", activateItemHon);
            
        }
    }
        
        
    if($('#referencesList').length)
    {
        var myrefList  = document.getElementById("referencesList").getElementsByTagName("tr");
        
         for(i=0; i < myrefList.length; i++)
        {
            myrefList[i].addEventListener("click", activateItemRef);
            
        }
    }
    
    if($('#skillsList').length)
    {
        var myskillList  = document.getElementById("skillsList").getElementsByTagName("tr");
        
         for(i=0; i < myskillList.length; i++)
        {
            myskillList[i].addEventListener("click", activateItemSkill);
            
        }
    }
        
       
    
    function activateItemExp()
    {
        var name = this.childNodes[5].innerHTML;
        var position = this.childNodes[7].innerHTML;
        document.getElementById("expLabel").innerHTML = name;
        document.getElementById("expBody").innerHTML = position;
        
    }
    
     function activateItemHon()
    {
        var name = this.childNodes[3].innerHTML;
        var descr = this.childNodes[5].innerHTML;
        document.getElementById("honLabel").innerHTML = name;
        document.getElementById("honBody").innerHTML = descr;
        
    }
    
     function activateItemRef()
    {
        var name = this.childNodes[1].innerHTML;
        var info = this.childNodes[3].innerHTML;
        document.getElementById("refLabel").innerHTML = name;
        document.getElementById("refBody").innerHTML = info;
        
    }
    
    function activateItemSkill()
    {
        var name = this.childNodes[1].innerHTML;
        var descr = this.childNodes[3].innerHTML;
        document.getElementById("skillLabel").innerHTML = name;
        document.getElementById("skillBody").innerHTML = descr;
        
    }
    
});
